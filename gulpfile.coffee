gulp = require 'gulp'
coffee = require 'gulp-coffee'
jade = require 'gulp-jade'
plumber = require 'gulp-plumber'

gulp.task 'coffee', ->
  gulp.src './**/*.coffee'
    .pipe plumber()
    .pipe coffee()
    .pipe gulp.dest('./')

gulp.task 'jade', ->
  gulp.src './**/*.jade'
    .pipe plumber()
    .pipe jade()
    .pipe gulp.dest('./')

gulp.task 'watch', ->
  gulp.watch './**/*.coffee', ['coffee']
  gulp.watch './**/*.jade', ['jade']

gulp.task 'default', ['coffee','jade','watch']
