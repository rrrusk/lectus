(function() {
  var Twitter,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Twitter = (function() {
    function Twitter() {
      this.accessor = {
        consumerSecret: 'I1TdoLVBV6kTwET7CA7cTzz2lUXdDxB9REhdXYyo6BDKw1J73s',
        tokenSecret: ''
      };
      this.message = {
        method: 'GET',
        parameters: {
          oauth_signature_method: 'HMAC-SHA1',
          oauth_consumer_key: 'E5nbR6DIZm9NREEhvxY0ny9vR'
        }
      };
    }

    Twitter.prototype.makeOauthUrl = function(message, accessor) {
      var url;
      OAuth.setTimestampAndNonce(message);
      OAuth.SignatureMethod.sign(message, accessor);
      url = OAuth.addToURL(message.action, message.parameters);
      return url;
    };

    return Twitter;

  })();

  this.TwitterAuth = (function(superClass) {
    extend(TwitterAuth, superClass);

    function TwitterAuth() {
      return TwitterAuth.__super__.constructor.apply(this, arguments);
    }

    TwitterAuth.prototype.httpGet = function(url, callback) {
      var http;
      http = require('https');
      return http.get(url, function(res) {
        var body;
        body = '';
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
          return body += chunk;
        });
        return res.on('end', function() {
          return callback(body);
        });
      });
    };

    TwitterAuth.prototype.getRequestToken = function() {
      var url;
      this.message.action = 'https://api.twitter.com/oauth/request_token';
      url = this.makeOauthUrl(this.message, this.accessor);
      return this.httpGet(url, function(body) {
        var gui, popWindow, query;
        query = {};
        body.split('&').map(function(x) {
          var pair;
          pair = x.split('=');
          return query[pair[0]] = pair[1];
        });
        localStorage.oauth_token = query['oauth_token'];
        localStorage.oauth_token_secret = query['oauth_token_secret'];
        url = 'https://api.twitter.com/oauth/authorize?oauth_token=' + query['oauth_token'];
        gui = require('nw.gui');
        return popWindow = gui.Window.open(url, {
          width: 480,
          height: 270
        });
      });
    };

    TwitterAuth.prototype.getAccessToken = function() {
      var url;
      this.accessor.tokenSecret = localStorage.oauth_token_secret;
      this.message.action = 'https://api.twitter.com/oauth/access_token';
      this.message.parameters.oauth_token = localStorage.oauth_token;
      this.message.parameters.oauth_verifier = $('#pin').val();
      url = this.makeOauthUrl(this.message, this.accessor);
      return this.httpGet(url, function(body) {
        var query;
        query = {};
        body.split('&').map(function(x) {
          var pair;
          pair = x.split('=');
          return query[pair[0]] = pair[1];
        });
        localStorage.accessToken = query['oauth_token'];
        localStorage.tokenSecret = query['oauth_token_secret'];
        localStorage.screenName = query['screen_name'];
        if (query['screen_name']) {
          return location.href = './timeline.html';
        }
      });
    };

    return TwitterAuth;

  })(Twitter);

  this.TwitterOperate = (function(superClass) {
    extend(TwitterOperate, superClass);

    function TwitterOperate() {
      TwitterOperate.__super__.constructor.call(this);
      this.message.parameters.oauth_token = localStorage.accessToken;
      this.accessor.tokenSecret = localStorage.tokenSecret;
      this.user = localStorage.screenName;
      this.list = {
        timeline: [],
        tweets: [],
        usersShow: []
      };
    }

    TwitterOperate.prototype.observeDataList = function(selector) {
      var _this;
      _this = this;
      return Object.observe(this.list, function(changes) {
        return changes.forEach(function(change) {
          return document.querySelector(selector)[change.name] = _this.list[change.name];
        });
      });
    };

    TwitterOperate.prototype.timeline = function() {
      var url;
      this.message.action = 'https://api.twitter.com/1.1/statuses/home_timeline.json';
      this.message.parameters.count = 100;
      url = this.makeOauthUrl(this.message, this.accessor);
      $.getJSON(url).done((function(_this) {
        return function(data) {
          return _this.list.timeline = data;
        };
      })(this));
      return this.list.timeline;
    };

    TwitterOperate.prototype.tweets = function(screen_name) {
      var url;
      this.message.action = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
      this.message.parameters.count = 100;
      this.message.parameters.screen_name = screen_name;
      url = this.makeOauthUrl(this.message, this.accessor);
      $.getJSON(url).done((function(_this) {
        return function(data) {
          return _this.list.tweets = data;
        };
      })(this));
      return this.list.tweets;
    };

    TwitterOperate.prototype.usersShow = function(screen_name) {
      var url;
      if (screen_name == null) {
        screen_name = localStorage.screenName;
      }
      this.message.action = 'https://api.twitter.com/1.1/users/show.json';
      this.message.parameters.screen_name = screen_name;
      url = this.makeOauthUrl(this.message, this.accessor);
      $.getJSON(url).done((function(_this) {
        return function(data) {
          return _this.list.usersShow = data;
        };
      })(this));
      return this.list.usersShow;
    };

    TwitterOperate.prototype.postTweet = function(content, media_ids) {
      var url;
      this.message.action = 'https://api.twitter.com/1.1/statuses/update.json';
      this.message.parameters.status = content;
      this.message.method = 'POST';
      url = this.makeOauthUrl(this.message, this.accessor);
      return $.ajax({
        type: this.message.method,
        url: url,
        dataType: 'json'
      });
    };

    TwitterOperate.prototype.mediaUpload = function(content, data) {
      var fd, url;
      this.message.action = 'https://upload.twitter.com/1.1/media/upload.json';
      this.message.method = 'POST';
      fd = new FormData();
      fd.append('media', data[0]);
      url = this.makeOauthUrl(this.message, this.accessor);
      return $.ajax({
        type: this.message.method,
        url: url,
        dataType: 'json',
        processData: false,
        contentType: false,
        data: fd
      }).done((function(_this) {
        return function(data) {
          console.log(data);
          _this.message.action = 'https://api.twitter.com/1.1/statuses/update.json';
          _this.message.parameters.status = "癒やされる(確信)";
          _this.message.parameters.media_ids = data.media_id_string;
          _this.message.method = 'POST';
          url = _this.makeOauthUrl(_this.message, _this.accessor);
          return $.ajax({
            type: _this.message.method,
            url: url,
            dataType: 'json'
          });
        };
      })(this));
    };

    TwitterOperate.prototype.favoritesCreate = function(id) {
      var url;
      this.message.action = 'https://api.twitter.com/1.1/favorites/create.json';
      this.message.method = 'POST';
      this.message.parameters.id = id;
      url = this.makeOauthUrl(this.message, this.accessor);
      return $.ajax({
        type: this.message.method,
        url: url,
        dataType: 'json'
      }).done(function(data) {
        return console.log(data);
      });
    };

    return TwitterOperate;

  })(Twitter);

  this.Lectus = (function() {
    function Lectus() {}

    Lectus.prototype.getRequestParameters = function(url) {
      var i, len, pair, parameters, query, ref;
      parameters = {};
      ref = url.split("?")[1].split("&");
      for (i = 0, len = ref.length; i < len; i++) {
        query = ref[i];
        pair = query.split("=");
        parameters[pair[0]] = pair[1];
      }
      return parameters;
    };

    return Lectus;

  })();

}).call(this);
