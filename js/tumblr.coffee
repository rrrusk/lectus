class Tumblr
  constructor: ->
    @accessor =
      consumerSecret: 'dXP8VfF2ZwcVOAFzaDvlFl0x5GQXN3rSWP4itJFPbr8po2Kpce'
      tokenSecret: ''

    @message =
      method: 'GET'
      parameters:
        oauth_signature_method: 'HMAC-SHA1'
        oauth_consumer_key: 'FHEDmmKcfp6wCerfZaz2gGCZgggUGe2dEsje3tGGi7v2ZsRzON'

  makeOauthUrl: (message,accessor) ->
    OAuth.setTimestampAndNonce(message)
    OAuth.SignatureMethod.sign(message, accessor)
    url = OAuth.addToURL(message.action, message.parameters)
    return url

class @TumblrAuth extends Tumblr
  httpGet: (url,callback) ->
    http = require('http')
    http.get url, (res) ->
      body = ''
      res.setEncoding 'utf8'
      res.on 'data', (chunk) ->
        body += chunk
      res.on 'end', ->
        callback(body)

  getRequestToken: ->
    @message.action = 'http://www.tumblr.com/oauth/request_token'
    url = @makeOauthUrl(@message,@accessor)

    http = require 'http'
    server = http.createServer()
    server.on 'request', (req, res) ->
      res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'})
      res.write(req.url)
      res.write('\n上の行をフォームに貼り付けて、accessボタンを押してください')
      res.end()
    .listen(3543)

    @httpGet url, (body) ->
      query = {}
      body.split('&').map (x) ->
        pair = x.split('=')
        query[pair[0]] = pair[1]
      localStorage.oauth_token = query['oauth_token']
      localStorage.oauth_token_secret = query['oauth_token_secret']
      url = 'http://www.tumblr.com/oauth/authorize?oauth_token=' + query['oauth_token']
      gui = require('nw.gui')
      popWindow = gui.Window.open url,
        width: 480
        height: 270

  getAccessToken: ->
    nurl = require "url"
    @accessor.tokenSecret = localStorage.oauth_token_secret
    @message.action = 'http://www.tumblr.com/oauth/access_token'
    @message.parameters.oauth_token = localStorage.oauth_token
    @message.parameters.oauth_verifier = nurl.parse($('#pin').val(),true).query.oauth_verifier
    url = @makeOauthUrl(@message,@accessor)
    @httpGet url, (body) ->
      query = {}
      body.split('&').map (x) ->
        pair = x.split('=')
        query[pair[0]] = pair[1]
      localStorage.accessToken = query['oauth_token']
      localStorage.tokenSecret = query['oauth_token_secret']

class @TumblrOperate extends Tumblr
  constructor: ->
    super()
    @message.parameters.oauth_token = localStorage.accessToken
    @accessor.tokenSecret = localStorage.tokenSecret

  dashboard: ->
    @message.action = 'http://api.tumblr.com/v2/user/dashboard'
    url = @makeOauthUrl(@message,@accessor)
    $.getJSON(url).done (data) =>
      console.log data

class @Lectus
  getRequestParameters: (url) ->
    parameters = {}
    for query in url.split("?")[1].split("&")
      pair = query.split("=")
      parameters[pair[0]] = pair[1]
    parameters
