(function() {
  var Tumblr,
    extend = function(child, parent) { for (var key in parent) { if (hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; },
    hasProp = {}.hasOwnProperty;

  Tumblr = (function() {
    function Tumblr() {
      this.accessor = {
        consumerSecret: 'dXP8VfF2ZwcVOAFzaDvlFl0x5GQXN3rSWP4itJFPbr8po2Kpce',
        tokenSecret: ''
      };
      this.message = {
        method: 'GET',
        parameters: {
          oauth_signature_method: 'HMAC-SHA1',
          oauth_consumer_key: 'FHEDmmKcfp6wCerfZaz2gGCZgggUGe2dEsje3tGGi7v2ZsRzON'
        }
      };
    }

    Tumblr.prototype.makeOauthUrl = function(message, accessor) {
      var url;
      OAuth.setTimestampAndNonce(message);
      OAuth.SignatureMethod.sign(message, accessor);
      url = OAuth.addToURL(message.action, message.parameters);
      return url;
    };

    return Tumblr;

  })();

  this.TumblrAuth = (function(superClass) {
    extend(TumblrAuth, superClass);

    function TumblrAuth() {
      return TumblrAuth.__super__.constructor.apply(this, arguments);
    }

    TumblrAuth.prototype.httpGet = function(url, callback) {
      var http;
      http = require('http');
      return http.get(url, function(res) {
        var body;
        body = '';
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
          return body += chunk;
        });
        return res.on('end', function() {
          return callback(body);
        });
      });
    };

    TumblrAuth.prototype.getRequestToken = function() {
      var http, server, url;
      this.message.action = 'http://www.tumblr.com/oauth/request_token';
      url = this.makeOauthUrl(this.message, this.accessor);
      http = require('http');
      server = http.createServer();
      server.on('request', function(req, res) {
        res.writeHead(200, {
          'Content-Type': 'text/plain; charset=utf-8'
        });
        res.write(req.url);
        res.write('\n上の行をフォームに貼り付けて、accessボタンを押してください');
        return res.end();
      }).listen(3543);
      return this.httpGet(url, function(body) {
        var gui, popWindow, query;
        query = {};
        body.split('&').map(function(x) {
          var pair;
          pair = x.split('=');
          return query[pair[0]] = pair[1];
        });
        localStorage.oauth_token = query['oauth_token'];
        localStorage.oauth_token_secret = query['oauth_token_secret'];
        url = 'http://www.tumblr.com/oauth/authorize?oauth_token=' + query['oauth_token'];
        gui = require('nw.gui');
        return popWindow = gui.Window.open(url, {
          width: 480,
          height: 270
        });
      });
    };

    TumblrAuth.prototype.getAccessToken = function() {
      var nurl, url;
      nurl = require("url");
      this.accessor.tokenSecret = localStorage.oauth_token_secret;
      this.message.action = 'http://www.tumblr.com/oauth/access_token';
      this.message.parameters.oauth_token = localStorage.oauth_token;
      this.message.parameters.oauth_verifier = nurl.parse($('#pin').val(), true).query.oauth_verifier;
      url = this.makeOauthUrl(this.message, this.accessor);
      return this.httpGet(url, function(body) {
        var query;
        query = {};
        body.split('&').map(function(x) {
          var pair;
          pair = x.split('=');
          return query[pair[0]] = pair[1];
        });
        localStorage.accessToken = query['oauth_token'];
        return localStorage.tokenSecret = query['oauth_token_secret'];
      });
    };

    return TumblrAuth;

  })(Tumblr);

  this.TumblrOperate = (function(superClass) {
    extend(TumblrOperate, superClass);

    function TumblrOperate() {
      TumblrOperate.__super__.constructor.call(this);
      this.message.parameters.oauth_token = localStorage.accessToken;
      this.accessor.tokenSecret = localStorage.tokenSecret;
    }

    TumblrOperate.prototype.dashboard = function() {
      var url;
      this.message.action = 'http://api.tumblr.com/v2/user/dashboard';
      url = this.makeOauthUrl(this.message, this.accessor);
      return $.getJSON(url).done((function(_this) {
        return function(data) {
          return console.log(data);
        };
      })(this));
    };

    return TumblrOperate;

  })(Tumblr);

  this.Lectus = (function() {
    function Lectus() {}

    Lectus.prototype.getRequestParameters = function(url) {
      var i, len, pair, parameters, query, ref;
      parameters = {};
      ref = url.split("?")[1].split("&");
      for (i = 0, len = ref.length; i < len; i++) {
        query = ref[i];
        pair = query.split("=");
        parameters[pair[0]] = pair[1];
      }
      return parameters;
    };

    return Lectus;

  })();

}).call(this);
