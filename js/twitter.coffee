class Twitter
  constructor: ->
    @accessor =
      consumerSecret: 'I1TdoLVBV6kTwET7CA7cTzz2lUXdDxB9REhdXYyo6BDKw1J73s'
      tokenSecret: ''

    @message =
      method: 'GET'
      parameters:
        oauth_signature_method: 'HMAC-SHA1'
        oauth_consumer_key: 'E5nbR6DIZm9NREEhvxY0ny9vR'

  makeOauthUrl: (message,accessor) ->
    OAuth.setTimestampAndNonce(message)
    OAuth.SignatureMethod.sign(message, accessor)
    url = OAuth.addToURL(message.action, message.parameters)
    return url

class @TwitterAuth extends Twitter
  httpGet: (url,callback) ->
    http = require('https')
    http.get url, (res) ->
      body = ''
      res.setEncoding 'utf8'
      res.on 'data', (chunk) ->
        body += chunk
      res.on 'end', ->
        callback(body)

  getRequestToken: ->
    @message.action = 'https://api.twitter.com/oauth/request_token'
    url = @makeOauthUrl(@message,@accessor)
    @httpGet url, (body) ->
      query = {}
      body.split('&').map (x) ->
        pair = x.split('=')
        query[pair[0]] = pair[1]
      localStorage.oauth_token = query['oauth_token']
      localStorage.oauth_token_secret = query['oauth_token_secret']
      url = 'https://api.twitter.com/oauth/authorize?oauth_token=' + query['oauth_token']
      gui = require('nw.gui')
      popWindow = gui.Window.open url,
        width: 480
        height: 270

  getAccessToken: ->
    @accessor.tokenSecret = localStorage.oauth_token_secret
    @message.action = 'https://api.twitter.com/oauth/access_token'
    @message.parameters.oauth_token = localStorage.oauth_token
    @message.parameters.oauth_verifier = $('#pin').val()
    url = @makeOauthUrl(@message,@accessor)
    @httpGet url, (body) ->
      query = {}
      body.split('&').map (x) ->
        pair = x.split('=')
        query[pair[0]] = pair[1]
      localStorage.accessToken = query['oauth_token']
      localStorage.tokenSecret = query['oauth_token_secret']
      localStorage.screenName = query['screen_name']
      if query['screen_name']
        location.href = './timeline.html'

class @TwitterOperate extends Twitter
  constructor: ->
    super()
    @message.parameters.oauth_token = localStorage.accessToken
    @accessor.tokenSecret = localStorage.tokenSecret
    @user = localStorage.screenName
    @list =
      timeline:[]
      tweets:[]
      usersShow:[]

  observeDataList: (selector) ->
    _this = @
    Object.observe @list, (changes) ->
      changes.forEach (change) ->
        document.querySelector(selector)[change.name] = _this.list[change.name]

  timeline: ->
    @message.action = 'https://api.twitter.com/1.1/statuses/home_timeline.json'
    @message.parameters.count = 100
    url = @makeOauthUrl(@message,@accessor)
    $.getJSON(url).done (data) =>
      @list.timeline = data
    return @list.timeline

  tweets: (screen_name) ->
    @message.action = 'https://api.twitter.com/1.1/statuses/user_timeline.json'
    @message.parameters.count = 100
    @message.parameters.screen_name = screen_name
    url = @makeOauthUrl(@message,@accessor)
    $.getJSON(url).done (data) =>
      @list.tweets = data
    return @list.tweets

  usersShow: (screen_name = localStorage.screenName) ->
    @message.action = 'https://api.twitter.com/1.1/users/show.json'
    @message.parameters.screen_name = screen_name
    url = @makeOauthUrl(@message,@accessor)
    $.getJSON(url).done (data) =>
      @list.usersShow = data
    return @list.usersShow

  postTweet: (content,media_ids) ->
    @message.action = 'https://api.twitter.com/1.1/statuses/update.json'
    @message.parameters.status = content
    @message.method = 'POST'
    url = @makeOauthUrl(@message,@accessor)
    $.ajax
      type: @message.method
      url: url
      dataType: 'json'

  #余裕出来たらpostTweet(stuatusUpdataに変更するかも)に統合
  # @message.parameters.media_ids = media_ids if media_ids
  mediaUpload: (content,data) ->
    @message.action = 'https://upload.twitter.com/1.1/media/upload.json'
    @message.method = 'POST'
    fd = new FormData()
    fd.append('media',data[0])
    url = @makeOauthUrl(@message,@accessor)
    $.ajax
      type: @message.method
      url: url
      dataType: 'json'
      processData: false
      contentType: false
      data: fd
    .done (data) =>
      console.log data
      @message.action = 'https://api.twitter.com/1.1/statuses/update.json'
      @message.parameters.status = "癒やされる(確信)"
      @message.parameters.media_ids = data.media_id_string
      @message.method = 'POST'
      url = @makeOauthUrl(@message,@accessor)
      $.ajax
        type: @message.method
        url: url
        dataType: 'json'

  favoritesCreate: (id) ->
    @message.action = 'https://api.twitter.com/1.1/favorites/create.json'
    @message.method = 'POST'
    @message.parameters.id = id
    url = @makeOauthUrl(@message,@accessor)
    $.ajax
      type: @message.method
      url: url
      dataType: 'json'
    .done (data) ->
      console.log data

class @Lectus
  getRequestParameters: (url) ->
    parameters = {}
    for query in url.split("?")[1].split("&")
      pair = query.split("=")
      parameters[pair[0]] = pair[1]
    parameters
