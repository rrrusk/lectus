(function() {
  var coffee, gulp, jade, plumber;

  gulp = require('gulp');

  coffee = require('gulp-coffee');

  jade = require('gulp-jade');

  plumber = require('gulp-plumber');

  gulp.task('coffee', function() {
    return gulp.src('./**/*.coffee').pipe(plumber()).pipe(coffee()).pipe(gulp.dest('./'));
  });

  gulp.task('jade', function() {
    return gulp.src('./**/*.jade').pipe(plumber()).pipe(jade()).pipe(gulp.dest('./'));
  });

  gulp.task('watch', function() {
    gulp.watch('./**/*.coffee', ['coffee']);
    return gulp.watch('./**/*.jade', ['jade']);
  });

  gulp.task('default', ['coffee', 'jade', 'watch']);

}).call(this);
